# When2Heat

This is a small helper package to download data related to heat demand in Europe from
[When2Heat](https://data.open-power-system-data.org/when2heat/latest/).

## Installation

If you have not already done so, add the
[daschw-lab](https://codeberg.org/daschw-lab/daschw-lab) registry to Julia.
Afterwards, you can use `Pkg` to install When2Heat.

```julia
pkg> add When2Heat
```

or

```julia
julia> import Pkg; Pkg.add("When2Heat")
```

## Configuration

By default, all downloaded data is stored in `~/.julia/data/When2Heat/`.
If you want to save it in a different location, you can set the
`WHEN2HEAT_DATA_PATH` environment variable, for example by adding the following line to
your Julia startup file `~/.julia/config/startup.jl`.

```julia
ENV["WHEN2HEAT_DATA_PATH"] = "/path/to/store/your/data"
```

## Usage

```julia
using When2Heat
```

The source data is stored in a quite large (about 280 MB) CSV file.
At the first usage of When2Heat, this dataset is downloaded and split into individual files per
country and year.
Hence, the first call of `using When2Heat` might take some time.
Subsequent calls will use the locally stored files and be significantly faster.
To update the local files you can run `When2Heat.update()`.

```julia
julia> When2Heat.update()
[ Info: Downloading When2Heat source data.
[ Info: Splitting source data in smaller files per country and year.
[ Info: Removing source data.
```

You can load heat and corresponding heat pump coefficient of performance (COP) profiles for a
specific `country` and `year` with `When2Heat.load(country, year)`.
Countries are referred to by the two-letter
[ISO 3166-1 alpha-2](https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2) country codes.
`When2Heat.load` returns a
[CSV.File](https://csv.juliadata.org/stable/reading.html#CSV.File)
object that can be wrapped for example in
[TypedTables.Table](https://typedtables.juliadata.org/latest/man/table/)s
or [DataFrames.DataFrame](https://dataframes.juliadata.org/stable/lib/types/#DataFrames.DataFrame)s.

```julia
julia> using DataFrames

julia> at20 = DataFrame(When2Heat.load(:AT, 2020))
8785×26 DataFrame
  Row │ utc_timestamp         cet_cest_timestamp        AT_COP_ASHP_floor  AT_COP_AS ⋯
      │ String31              String31                  Float64            Float64   ⋯
──────┼───────────────────────────────────────────────────────────────────────────────
    1 │ 2019-12-31T23:00:00Z  2020-01-01T00:00:00+0100              2.545            ⋯
    2 │ 2020-01-01T00:00:00Z  2020-01-01T01:00:00+0100              2.5
    3 │ 2020-01-01T01:00:00Z  2020-01-01T02:00:00+0100              2.47
  ⋮   │          ⋮                       ⋮                      ⋮                    ⋱
 8783 │ 2020-12-31T21:00:00Z  2020-12-31T22:00:00+0100              2.27
 8784 │ 2020-12-31T22:00:00Z  2020-12-31T23:00:00+0100              2.25             ⋯
 8785 │ 2020-12-31T23:00:00Z  2021-01-01T00:00:00+0100              2.25
                                                      23 columns and 8779 rows omitted

julia> ro17 = DataFrame(When2Heat.load("RO", 2017))
8761×26 DataFrame
  Row │ utc_timestamp         cet_cest_timestamp        RO_COP_ASHP_floor  RO_COP_AS ⋯
      │ String31              String31                  Float64            Float64   ⋯
──────┼───────────────────────────────────────────────────────────────────────────────
    1 │ 2016-12-31T23:00:00Z  2017-01-01T00:00:00+0100              1.995            ⋯
    2 │ 2017-01-01T00:00:00Z  2017-01-01T01:00:00+0100              1.96
    3 │ 2017-01-01T01:00:00Z  2017-01-01T02:00:00+0100              1.96
  ⋮   │          ⋮                       ⋮                      ⋮                    ⋱
 8759 │ 2017-12-31T21:00:00Z  2017-12-31T22:00:00+0100              2.8
 8760 │ 2017-12-31T22:00:00Z  2017-12-31T23:00:00+0100              2.795            ⋯
 8761 │ 2017-12-31T23:00:00Z  2018-01-01T00:00:00+0100              2.785
                                                      23 columns and 8755 rows omitted
```

module When2Heat

import CSV
import Downloads

"""
    When2Heat.load(country, year)

Load data for `country` and `year` in a `CSV.File`.
This can be used to construct for example a `DataFrame` or a `TypedTables.Table`.
"""
function load(country, year)
    return CSV.File(
        joinpath(DATA_PATH, string(country, "_", year, ".csv")),
        delim=';',
        decimal=',',
    )
end

"""
    When2Heat.update()

Download the original CSV data file from
https://data.open-power-system-data.org/when2heat/latest/when2heat.csv
and split into smaller files per country and year.
"""
function update()
    url = "https://data.open-power-system-data.org/when2heat/latest/when2heat.csv"
    src_file = joinpath(DATA_PATH, "when2heat.csv")
    @info "Downloading When2Heat source data."
    Downloads.download(url, src_file)
    @info "Splitting source data in smaller files per country and year."
    split_source()
    @info "Removing source data."
    rm(src_file)
end

function split_source()
    lines = readlines(joinpath(DATA_PATH, "when2heat.csv"))
    for (year, year_lines) in split_years(lines)
        for (country, country_lines) in split_countries(year_lines)
            small_fn = joinpath(DATA_PATH, string(country, "_", year, ".csv"))
            open(small_fn, "w") do io
                map(country_lines) do line
                    println(io, line)
                end
            end
        end
    end
end

function split_years(lines)
    utc_year(line) = parse(Int, line[1:4])
    cet_year(line) = parse(Int, line[22:25])
    years = utc_year(lines[2])+1:utc_year(lines[end])
    header = first(lines)
    n = length(lines)
    return Dict(
        year => [
            header
            filter(
                line -> cet_year(line) >= year && utc_year(line) <= year,
                view(lines, 2:n)
            )
        ] for year in years
    )
end

function split_countries(lines)
    header = lines[1]
    header_names = split(header, ';')
    header_countries = map(name -> first(split(name, '_')), header_names)
    countries = unique(header_countries[3:end])
    country_cols = Dict(
        country => findall(==(country), header_countries) for country in countries
    )
    return Dict(
        country => map(lines) do line
            fields = split(line, ';')
            inds = [1:2; country_cols[country]]
            join(fields[inds], ';')
        end for country in countries
    )
end

function __init__()
    default_path = joinpath(homedir(), ".julia", "data", "When2Heat")
    global DATA_PATH = get(ENV, "WHEN2HEAT_DATA_PATH", default_path)
    mkpath(DATA_PATH)
    isfile(joinpath(DATA_PATH, "AT_2020.csv")) || update()
end

end
